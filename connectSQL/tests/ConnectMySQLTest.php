<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\ConnectMySQL;
use App\ConnectPostgres;

class ConnectMySQLTest extends TestCase
{
	public function testconnect(){
	    $rs = true;
	    $conn = (new ConnectMySQL())->createConnect();
	    if(!$conn)
	        $rs = false;
		    $this->assertTrue($rs);
	}
	public function testconnectPostgres(){
	    $rs = true;
	    $conn = ConnectPostgres::connectDBPostgres();
	    if(!$conn)
	        $rs = false;
		    $this->assertTrue($rs);
	}
}
