<?php
    namespace App;

    class ConnectMySQL
    {
        public $servername = "mariadb";
        public $username = "root";
        public $password = "123";

        public function createConnect(){
            // Create connection
            $conn = mysqli_connect($this->servername, $this->username, $this->password);
            return $conn;
        }

    }
